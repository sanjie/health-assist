<?php
/**
 *
 * @author 凡墙<jihaoju@qq.com>
 * @date 2018/7/12 10:56
 * @description
 */
return [
//    'course_poster/submit' => 'login',
//    'course_order/*' => 'login',
    'test/*' => 'non',
    'consigneer/*' => 'login',
    'health_assistant/*' => 'login',
    'health_assistant_order/*' => 'login',
    'health_service_order/*' => 'login',
    'member/*' => 'login',
    'message/*' => 'login',
    'pay_trade/create' => 'login',
    'realname_verify_task/*' => 'login',
    'statistics/*' => 'non',
    'user/save_msg_template' => 'login',
    'user_account/*' => 'login',
    'user_setting/*' => 'login',
    'user_withdraw_cash/*' => 'login',
];