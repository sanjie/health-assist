<?php

namespace apps\health_assist\app\mp\controller;

class FeedbackController extends BaseHealthAssistMpController
{

    public function submit()
    {
        return $this->success();
    }
}