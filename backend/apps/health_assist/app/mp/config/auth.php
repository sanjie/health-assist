<?php
/**
 *
 * @author 凡墙<jihaoju@qq.com>
 * @date 2018/7/12 10:56
 * @description
 */
return [
//    'course_poster/submit' => 'login',
//    'course_order/*' => 'login',
    'test/*' => 'non',
    'consigneer/*' => 'login',
    'order/*' => 'login',
    'pay/*' => 'login',
    'user_setting/*' => 'login',
    'user_product/*' => 'login',
    'user_salary/*' => 'login',
    'user_account/*' => 'login',
    'user_withdraw_cash/*' => 'login',
    'user/save_msg_template' => 'login',
    'statistics/*' => 'login',
    'group_red_envelope/give' => 'login',
    'group_red_envelope/draw' => 'login',
];