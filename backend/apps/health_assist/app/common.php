<?php
// 应用公共文件
define('SERVICE_NAMESPACE', 'apps\health_assist\core\service\\');
define('LOGIC_NAMESPACE', 'apps\health_assist\core\logic\\');

require_once __DIR__ . '/../core/constants.php';
require_once __DIR__ . '/../../../extend/vm/functions.php';
require_once __DIR__ . '/events.php';
