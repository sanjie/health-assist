<?php
return [
    'default' => [
        'ios' => 'jTXpFmqQKGeXwjzmkyOlp7eGNllHJnS4',
        'android' => 'VU0lN4dvbTiqWHRW1NcP9suqap7UGFYO',
        'miniapp' => '3iOV3qSISl1XgJwhXKSNPP9kZi5UqToW'
    ],
    'level_1' => [
        'ios' => 'J9vKrcASip43NEtMn8uTAbqn2aoJZBto',
        'android' => '2UI9xVPcmicX86tr2amFL7qbBV5NLbfi',
        'miniapp' => 'eWZ4nGkgy3Xqu6vX81pXTZypcUSgQjWH'
    ],
];