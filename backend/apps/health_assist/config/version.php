<?php
/**
 *
 * @author 凡墙<jihaoju@qq.com>
 * @date 2018/7/11 17:17
 * @description
 */

return [
    'version' => '2.0.0',   // 系统版本号
    'release' => '2022年06月16日'
];