//
// @author 凡墙<jihaoju@qq.com>
// @date 2018/3/20 15:11
// @description

var BASE_URL_DEV = '"/op.php"';
var BASE_FILE_URL_DEV = '"/upload.php"';
var BASE_STATIC_URL_DEV = '"/health_assist/"';

var BASE_URL_RELEASE = '"/op.php"';
var BASE_FILE_URL_RELEASE = '"/upload.php"';
var BASE_STATIC_URL_RELEASE = '"/health_assist/"';

module.exports = {
  BASE_URL_DEV: BASE_URL_DEV,
  BASE_URL_RELEASE: BASE_URL_RELEASE,
  BASE_FILE_URL_DEV: BASE_FILE_URL_DEV,
  BASE_FILE_URL_RELEASE: BASE_FILE_URL_RELEASE,
  BASE_STATIC_URL_DEV: BASE_STATIC_URL_DEV,
  BASE_STATIC_URL_RELEASE: BASE_STATIC_URL_RELEASE,
  BASE_PATH_DEV: '/health_assist/op/',
  BASE_PATH_RELEASE: '/health_assist/op/'
};
