/**
 * Author: 凡墙<jihaoju@qq.com>
 * Time: 2017-8-9 14:14
 * Description:
 */
export const increment = ({commit}) => {
  commit('INCREMENT')
}
export const decrement = ({commit}) => {
  commit('DECREMENT')
}
