
export default {
  wxUserInfo: null,
  userInfo: null,
  memberProfile: null,
  siteConfig: null,
  event: {},
  ws: {
    connected: false,
    subscribed: false
  },
}
