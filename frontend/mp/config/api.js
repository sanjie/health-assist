//
// @author 凡墙<jihaoju@qq.com>
// @date 2018/3/20 15:11
// @description

var BASE_URL_DEV = '"/mp.php"';
var BASE_FILE_URL_DEV = '"/file/upload.php"';
var BASE_STATIC_URL_DEV = '"/static/health/"';

var BASE_URL_RELEASE = '"/mp.php"';
var BASE_FILE_URL_RELEASE = '"/upload.php"';
var BASE_STATIC_URL_RELEASE = '"/static/health/"';

module.exports = {
  BASE_URL_DEV: BASE_URL_DEV,
  BASE_FILE_URL_DEV: BASE_FILE_URL_DEV,
  BASE_URL_RELEASE: BASE_URL_RELEASE,
  BASE_FILE_URL_RELEASE: BASE_FILE_URL_RELEASE,
  BASE_STATIC_URL_DEV: BASE_STATIC_URL_DEV,
  BASE_STATIC_URL_RELEASE: BASE_STATIC_URL_RELEASE,
  BASE_PATH_DEV: '/health/mp/',
  BASE_PATH_RELEASE: '/health/mp/'
};
