//
// @author 凡墙<jihaoju@qq.com>
// @date 2020/07/01 21:00
// @description

var BASE_URL_DEV = '"http://local/dev/tp-boot/api/base/op.php"';
var BASE_FILE_URL_DEV = '"http://local/dev/tp-boot/api/file/upload.php"';

var BASE_URL_RELEASE = '"//api.server.cn/op.php"';
var BASE_FILE_URL_RELEASE = '"//api.server.cn/upload.php"';

var BASE_PATH_DEV = '/base/op/';
var BASE_PATH_RELEASE = '/base/op/';

module.exports = {
  BASE_URL_DEV: BASE_URL_DEV,
  BASE_URL_RELEASE: BASE_URL_RELEASE,
  BASE_FILE_URL_DEV: BASE_FILE_URL_DEV,
  BASE_FILE_URL_RELEASE: BASE_FILE_URL_RELEASE,
  BASE_PATH_DEV: BASE_PATH_DEV,
  BASE_PATH_RELEASE: BASE_PATH_RELEASE
};
