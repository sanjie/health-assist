/**
 * Author: 凡墙<jihaoju@qq.com>
 * Time: 2017-8-9 14:19
 * Description:
 */
// 定义所需的 mutations
export default {
  INCREMENT(state) {
    state.count++
  },
  DECREMENT(state) {
    state.count--
  }
}
