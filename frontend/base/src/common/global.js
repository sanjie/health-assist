
export default {
  userInfo: null,
  authorizedResources: [],
  navigations: [],
  homeActiveFirstNav: null,
  homeActiveSecondNav: null
}
